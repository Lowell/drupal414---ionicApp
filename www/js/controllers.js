angular.module('starter.controllers', ['starter.services'])

  .controller('ListCtrl', function($scope, $http, decodeHtml) {
    $http({
      method: 'GET',
      url: 'http://drupal414.version30.com/drupallers'
    }).success(function (data) {

      //console.log(data);

      for (var i = 0; i < data.length; ++i) {

        //data[i].name = decodeHtml.decodeHtml(data[i].name);

        var j =i+1;
        data[i].id = j;
      }

      $scope.list = data;

      //console.log('ListCtrl');

    });
  })

  .controller('ProfileCtrl', function($scope, $http, $stateParams, $state, decodeHtml) {
    $http({
      method: 'GET',
      url: 'http://drupal414.version30.com/drupallers'
    }).success(function (data) {

      for (var i = 0; i < data.length; ++i) {

        var j = i+1;
        data[i].id = j;

        data[i].user_bio2 = decodeHtml.decodeHtml(data[i].user_bio);

        if ($stateParams.profileId == data[i].id) {
          if (data[i].user_picture == "") { data[i].user_picture = ' no image '; }
          $scope.details = data[i];
        }
        console.log($scope.details);
      }
    });

    $scope.onSwipeRight = function (id) {
      id=id-1;
      $state.go('profile', { profileId: id });
    };

    $scope.onSwipeLeft = function (id) {
      id=id+1;
      $state.go('profile', { profileId: id });
    };

    //console.log('ProfileCtrl');

  });

